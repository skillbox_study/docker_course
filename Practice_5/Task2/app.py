from flask import Flask
import redis 
import socket

redis = redis.Redis(host='redis', port=6379)
app = Flask(__name__)
@app.route('/')
def hello ():
    count = redis.incr('hits')
    return 'Skillbox rules. <p> Hostname is: ' + socket.gethostname() + '\nI have been seen {} times. \n'.format(count)
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)